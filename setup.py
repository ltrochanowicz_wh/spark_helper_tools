import setuptools

setuptools.setup(
    name="retention-spark-jobs",
    version="0.0.4",
    author="Andrzej Lewcun",
    author_email="author@example.com",
    description="Retention team spark jobs",
    long_description="Retention team spark jobs with dependencies",
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(exclude=["databricks.tests", "databricks.tests.*", ""]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
