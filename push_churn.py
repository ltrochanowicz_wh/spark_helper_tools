import calendar
import sys
import json
import datetime
import time

FORMAT = "%Y-%m-%d %H:%M:%S"

sys.path.insert(1, '/Users/ltrochanowicz/PycharmProjects/smart-data-platform-logical')
from kafka import KafkaProducer


def get_producer():
    return KafkaProducer(bootstrap_servers='localhost:9092', batch_size=1, linger_ms=0)


def push_all(events, producer):
    for value in events:
        json_event = json.dumps(value)
        producer.send("nonprod-riga-uk-all", value=bytes(json_event, 'UTF-8'))
    producer.flush()


def get_events():
    return [{
        "transactionTimeUtc": "2020-03-01T00:00:00.000+0000",  # 1573044367136 /1000
        "accountNumber": "93290NL",
        "amount": 10.00,
        "balance": 200,
    },
        {
            "transactionTimeUtc": "2020-03-01T00:00:05.000+0000",  # 1573044367136
            "accountNumber": "93290NL",
            "amount": 11.00,
            "balance": 210,
        },
        {
            "transactionTimeUtc": "2020-03-01T00:00:10.000+0000",  # 1573044367136
            "accountNumber": "93290NL",
            "amount": 12.00,
            "balance": 220,
        },
        {
            "transactionTimeUtc": "2020-03-01T00:00:20.000+0000",  # 1573044367136
            "accountNumber": "93290NL",
            "amount": 10.00,
            "balance": 200,
        },
        {
            "transactionTimeUtc": "2020-03-01T00:00:30.000+0000",  # 1573044367136
            "accountNumber": "93290NL",
            "amount": 20.00,
            "balance": 300,
        },
        {
            "transactionTimeUtc": "2020-03-01T00:00:40.000+0000",  # 1573044367136
            "accountNumber": "93290NL",
            "amount": 21.00,
            "balance": 310,
        },
        {
            "transactionTimeUtc": "2020-03-01T00:00:50.000+0000",  # 1573044367136
            "accountNumber": "93290NL",
            "amount": 10.00,
            "balance": 200,
        },
        {
            "transactionTimeUtc": "2020-03-01T00:01:00.000+0000",  # 1573044367136
            "accountNumber": "93290NL",
            "amount": 22.00,
            "balance": 320,
        }]


def push_with_sleep(sleeps, producer, account="25355DL", game="Roulette"):
    event = {
        "operation": "CREATE_TRANSACTION",
        "accountNumber": account,
        "gameIdentifier": "3DRoulette",
        "territory": "COM",
        "currencyCode": "GBP",
        "balance": 1000.00,
        "amount": 1,
        "gameType": game,
        "transactionType": "STAKE",
        "transactionTimeUtc": ""
    }
    next = 100
    for value in sleeps:
        time.sleep(value)
        next += value
        event['transactionTimeUtc'] = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0).isoformat()
        event['balance'] = next
        event['accountNumber'] = account
        json_event = json.dumps(event)
        print(json_event)
        producer.send("nonprod-riga-uk-all", value=bytes(json_event, 'UTF-8'))


if __name__ == "__main__":
    producer = get_producer()
    now = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0).isoformat()
    print(now)
    push_with_sleep([1 for i in range(10)], producer)
    push_with_sleep([1 for i in range(10)], producer, "25355DL", game="Slots")
    producer.flush()
