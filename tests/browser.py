import time
import logging
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
import requests


def send_message_from_spark():
    payload = {'event_time': '2020-03-05T08:54:09.706+01:00', 'accountNo': '27139RN', 'amount_won': '1',
               'stake_amount': '9.00', 'obid': 'OB_EV2965596', 'outcome_marketName': '90 Minutes',
               'outcome_eventName': '2965595', 'outcome_marketRef': '9534696',
               'creationDate': '2019-12-10T01:27:36.000+0000', 'settledAt': '2019-12-10T01:28:21.000+0000',
               'tealium_account': 'williamhillgroup', 'tealium_profile': 'uk-wh', 'tealium_event': 'retention-test',
               'config_time': '2020-03-05T08:54:09.706+01:00', 'UVSTier': ['Tier 1', 'Tier 6'], 'MinBetAmount': '',
               'MaxBetAmount': '', 'targetStart': '2020-03-02T19:45:00.000Z', 'targetId': 'OB_EV2965596',
               'futureId': 'OB_EV2967738', 'futureStart': '2020-02-15T15:00:00.000Z',
               'futureName': 'Preston vs Millwall'}
    requests.post('https://collect.tealiumiq.com/event', json=payload,
                  headers={'Accept': 'application/json', 'Accept-Charset': 'utf-8', 'User-Agent': 'solutions-servers'})


console = logging.StreamHandler()
logging.getLogger('').addHandler(console)
logging.getLogger('').setLevel('DEBUG')
opts = Options()
opts.headless = True
browser = Firefox(options=opts)
browser.get('https://sports.williamhill-pp2.com/betting/en-gb')

try:
    browser.find_element_by_tag_name('div')
except:
    print('page not loaded')
    exit()

join = browser.find_element_by_id('accountTabButton')
join.click()

username = browser.find_element_by_id('loginUsernameInput')
username.send_keys('RETXXXX__03')
password = browser.find_element_by_id('loginPasswordInput')
password.send_keys('password1234')
loginButton = browser.find_element_by_id('loginButton')
loginButton.click()
print('logging in')
time.sleep(5)

send_message_from_spark()
for i in range(1,10):
    try:
        # browser.refresh() ??
        popup = browser.find_element_by_id('popup')
        print('popup found')
        exit()
    except:
        print('waiting for popup')
        time.sleep(15)
exit('POPUP NOT FOUND')