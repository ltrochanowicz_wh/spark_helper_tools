from kafka import KafkaConsumer, TopicPartition

if __name__ == "__main__":
    client = KafkaConsumer(bootstrap_servers='localhost:9092')
    tp = TopicPartition('marketing-openbet-bet-raw', 0)
    client.assign([tp])
    #
    count = client.position(tp)
    print(client.partitions_for_topic('marketing-openbet-bet-raw'))
    print(count)