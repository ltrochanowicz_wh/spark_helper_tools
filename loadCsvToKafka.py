import sys
sys.path.insert(1, '/Users/ltrochanowicz/PycharmProjects/smart-data-platform-logical')

from pyspark.sql import SparkSession
from pyspark.sql.functions import col, to_json, expr, struct
from pyspark.sql.types import StringType, StructType, StructField, MapType, ArrayType

if __name__ == '__main__':
    session = SparkSession \
        .builder \
        .appName("load_to_kafka") \
        .config("spark.jars.packages","org.apache.spark:spark-avro_2.11:2.4.4,org.apache.spark:spark-streaming-kafka-0-10_2.11:2.4.4,org.apache.spark:spark-sql-kafka-0-10_2.11:2.4.4") \
        .getOrCreate()

    query2 = session.read.format("avro").load("/Users/ltrochanowicz/Downloads/import/*")

    query2.show()
    query2.selectExpr('cast(null as string) as key', 'value') \
         .write \
         .format('kafka') \
         .option('kafka.bootstrap.servers', 'kafka:9092') \
         .option('topic', 'nonprod-riga-uk-all') \
         .save()