import boto3
from envparse import Env



if __name__ == "__main__":
    env = Env()
    env.read_envfile()
    # client = boto3.client('apigateway')
    client = boto3.client('secretsmanager')

    print('apis:')
    apis = client.list_secrets()
    print(apis)

    # print('resources:')
    # resources = client.get_resources(restApiId='o85q9uq00i')
    # print(resources)
    #
    # print('integrations:')
    # integration = client.get_integration(
    #     restApiId='o85q9uq00i',
    #     resourceId='0m8aph',
    #     httpMethod='POST'
    # )
    # print(integration)
    #
    # print('create api')
    # # response = client.create_rest_api(name='lt-boto-api') #e4n4bvp916
    # # print(response)
    #
    # print('get_resources')
    # get_resources = client.get_resources(restApiId='e4n4bvp916')
    # print(get_resources)

    # print('create resource')
    # response = client.create_resource(restApiId='e4n4bvp916', parentId='17yfw7v6w6', pathPart= 'some_res') #'id': 'nobwec'
    # print(response)

    # print('put_method')
    # requestParameters={"method.request.querystring.param1":True,"method.request.querystring.param2":False}
    # response = client.put_method(restApiId='e4n4bvp916', resourceId='nobwec', httpMethod='GET', authorizationType='NONE',
    #                              requestParameters=requestParameters)
    # print(response)
    #
    # print('put_integration')
    # response = client.put_integration(restApiId='e4n4bvp916', resourceId='nobwec', httpMethod='GET', type='MOCK')
    # print(response)