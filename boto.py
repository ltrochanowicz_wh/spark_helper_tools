import sys
from collections import namedtuple
from datetime import datetime, timedelta
from typing import List, Dict

sys.path.insert(1, '/Users/ltrochanowicz/PycharmProjects/smart-data-platform-logical/astronomer')
from dags.common.repository import list_temporal, SSMRepository, \
     Repository, S3BotoRepository
from dags.common.repository.codec import Codec, GzipCodec
from dags.common.repository import AwsAuth
from dags.common.util import not_null, from_kwargs, string_to_datetime

import json

FORMAT = '%Y-%m-%d'
LAST_PROCESSED_FILE_KEY = 'last_processed_log'
AUTH=AwsAuth(
            aws_access_key_id='ASIAQCWPFSNLDDNUJOOB',
            aws_secret_access_key='9k1Rj3kXvGQhSQBFjIvdY/RnvM+EiRHp3FgIoC/l',
            session='FwoGZXIvYXdzEJv//////////wEaDOCIfW8KJRREjVGozSKKAsD5rXbMjsGsCxqns50Oy0Sg4l8Mo3JYNFrmgsMqHj26JbwjZ8VrB5Dyv+5xmNumKlWV2PXR6t3Z9hIpRmfGJ8L6OZOsCS08/lQwaPM2uHD/gzgwutxtK3M5XdxSnT5E21+w0Is7a6ZKSRy1ezudIUn4BSnrjxOkFLlf9muJQ0xfhE9wqpyzDbTlSiKLbiu6vL+RvhPXyhdUO3ZXn6GYHUpZ0zvzlQ0lXS9NyFJBh1H1stt1SNCG+AUMNvNyzsUZ2RjHArxfkVJGRA0zZVZi5yPND78aK312QHcMsHyjGCJW9lInjUrkhERDOzRI/XDgLs1MvMvIxUeGa7C3HAoBMDht1ovzaZ9bFFE6KIGKsvcFMit9V6EVqOi+x4MjD84w+k1W4VMJd2Ck7RRQUHQvWpDpqHwOq+Ay4UepfhBg'
)



def _get_file_list(repo: Repository, from_date: datetime = None, to_date: datetime = None):
    last_processed = _metadata_repo().get(key=LAST_PROCESSED_FILE_KEY)
    if (from_date is None) & (last_processed is not None):
        from_date = string_to_datetime(last_processed[5:15], FORMAT)
    if to_date is None:
        to_date = datetime.now()
    print(
        'Getting files from: {from_date} to: {to_date}. Last processed: {last}'.format(from_date=from_date,
                                                                                       to_date=to_date,
                                                                                       last=last_processed))
    returned_list = repo.list('date=') if from_date is None else list_temporal(repo, from_date, to_date,
                                                                               'date={year}-{month}-{day}')
    files_list = sorted(returned_list)

    index = 0 if last_processed not in files_list else files_list.index(last_processed) + 1
    return files_list[index:]


def _metadata_repo() -> Repository:
    return SSMRepository(prefix='dag_id', auth=AUTH)


if __name__ == '__main__':
    repo = S3BotoRepository('wh-data-sdp-nonprod-eu-west-1-s3-ingest-logs-databricks', codec=GzipCodec()
                            , auth=AUTH)
    from_date = string_to_datetime('2020-06-15','%Y-%m-%d')
    to_date = string_to_datetime('2020-06-15','%Y-%m-%d')
    repo.delete('date=2020-06-15/part-93.json.gz')
    print(_metadata_repo().delete(LAST_PROCESSED_FILE_KEY))
    print(_metadata_repo().list())
    files_list = _get_file_list(repo, from_date, to_date)
    print(files_list)
    # _metadata_repo().put(LAST_PROCESSED_FILE_KEY, files_list[-1])


