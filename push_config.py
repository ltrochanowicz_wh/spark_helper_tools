import calendar
import sys
import json
import time

sys.path.insert(1, '/Users/ltrochanowicz/PycharmProjects/smart-data-platform-logical')
from kafka import KafkaProducer


def get_producer():
    return KafkaProducer(bootstrap_servers='localhost:9092', batch_size=1, linger_ms=0)


def push_all(events, producer):
    for value in events:
        json_event = json.dumps(value)
        producer.send("marketing-winning-bet-targets", value=bytes(json_event, 'UTF-8'))
    producer.flush()


def push_new(producer, list):
    for i in list:
        print(i)
        value = {"CVMTier": [], "MinBetAmount": "0", "SportName": "American Football", "SportID": "OB_SP1",
                 "MaxBetAmount": "100",
                 "SportClassID": "OB_CL19",
                 "TargetedEvent": {"StartDateTime": "2019-12-31T21:30:00.000Z", "Name": "Testing OB_EV2887615",
                                   "ID": "OB_EV2887615"},
                 "FutureEvent": {"StartDateTime": "2020-01-01T22:00:00.000Z", "Name": "Testing OB_EV2887615",
                                 "ID": "OB_EV2887614"}}

        value["TargetedEvent"]["ID"] = i
        json_event = json.dumps(value)
        producer.send("marketing-winning-bet-targets", value=bytes(json_event, 'UTF-8'))
        # time.sleep(20)
    producer.flush()


def push_churn_config(producer):
    value = {"AccountBalanceAmountTo": 5000, "AccountBalanceAmountFrom": 0, "GameType": "Slots"}
    json_event = json.dumps(value)
    producer.send("marketing-churn-targets", value=bytes(json_event, 'UTF-8'))
    producer.flush()

if __name__ == "__main__":
    producer = get_producer()
    # push_churn_config(producer)
    # events = main_pr.get_events()

    # push_all(events, producer)


    list = [
        "OB_EV619069",
        "OB_EV619214",
        "OB_EV619354",
        "OB_EV619493",
        "OB_EV619670",
        "OB_EV619804",
        "OB_EV619914",
        "OB_EV620055",
        "OB_EV619354",
        "OB_EV620055",
        "OB_EV619493",
        "OB_EV619804",
        "OB_EV619214",
        "OB_EV619670",
        "OB_EV619914",
        "OB_EV619214",
        "OB_EV619914",
        "OB_EV619670",
        "OB_EV600601",
        "OB_EV600620"
    ]
    push_new(producer, list)
