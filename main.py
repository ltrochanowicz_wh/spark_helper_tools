import os
import sys
import calendar
import time
import json
sys.path.insert(1, '/Users/ltrochanowicz/PycharmProjects/smart-data-platform-logical/databricks/streams/sdp_features')
import sdp_features.jobs.common.customer_pb2 as customer_pb2
from kafka import KafkaProducer

# aws-okta exec cs-dev -- aws s3 sync s3://wh-data-sdp-nonprod-eu-west-1-s3-ingest-uno/AF/proto/bets .
def readMessages(producer=None):
    for r, d, f in os.walk('/Users/ltrochanowicz/PycharmProjects/HelperProject/bets2'):
        for file in f:
            with open(os.path.join(r, file), 'rb') as cf:
                data = cf.read()
                a = customer_pb2.core__pb2.Activity()
                a.ParseFromString(data)
                print(file)
                print(str(a))
                # for field, value in a.ListFields():
                #     print(field.full_name, value)
                # print(a.Extensions[bet_pb2.state].bets[0].id)
                # print(a.Extensions[bet_pb2.state].bets[0])
                # print(a.Extensions[bet_pb2.state].bets[0].leg[0].legParts[0].eventRef)
                # print(a.Extensions[bet_pb2.supportingState].outcome[0].eventName)
                # print('-------------------------')
                producer and producer.send('marketing-openbet-bet-raw', value=a.SerializeToString())

    producer.flush()
    producer.close()



def prepareMessage(producer=None):
    for i in range(1,5):
        message = bet_pb2.core__pb2.Activity()
        message.header.contextRef='BET.SETTLEMENT'
        message.header.activityId=i
        message.header.timeStamp=calendar.timegm(time.gmtime())
        message.Extensions[bet_pb2.state].bets.add()
        message.Extensions[bet_pb2.state].bets[0].id="9147525035"
        message.Extensions[bet_pb2.state].bets[0].accountRef="27139RN"
        message.Extensions[bet_pb2.state].bets[0].customerRef="715446"
        message.Extensions[bet_pb2.state].bets[0].betTypeRef="SGL"
        message.Extensions[bet_pb2.state].bets[0].placedAt=""
        message.Extensions[bet_pb2.state].bets[0].settledAt="2019-12-10T01:28:21.000+0000"
        message.Extensions[bet_pb2.state].bets[0].creationDate="2019-12-10T01:27:36.000+0000"

        producer.send('marketing-openbet-bet-raw', value=message.SerializeToString())
        print(message.header.activityId)
        time.sleep(1)

    producer.flush()
    producer.close()





if __name__ == "__main__":
    producer = KafkaProducer(bootstrap_servers='localhost:9092', batch_size=1, linger_ms=0)
    readMessages(producer)
    # prepareMessage(producer)




