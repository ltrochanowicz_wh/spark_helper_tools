import json
import sys

from pyspark.sql.functions import from_json
from pyspark.sql.types import StructType, StructField, StringType

sys.path.insert(1, '/Users/ltrochanowicz/PycharmProjects/smart-data-platform-logical')
from databricks.dbfs.marketing.common import envs
from pyspark.sql import SparkSession, col



if __name__ == "__main__":
    # envs.log_env()

    schema = StructType([
    StructField('operation', StringType(), True),
    StructField('territory', StringType(), True),
    StructField('regulatorySessionId', StringType(), True),
    StructField('gameIdentifier', StringType(), True),
    StructField('tableIdentifier', StringType(), True),
    StructField('gameRound', StringType(), True),
    StructField('supplier', StringType(), True),
    StructField('channel', StringType(), True),
    StructField('brand', StringType(), True),
    StructField('accountNumber', StringType(), True),
    StructField('externalTransactionIdentifier', StringType(), True),
    StructField('internalTransactionIdentifier', StringType(), True),
    StructField('gameplayId', StringType(), True),
    StructField('transactionType', StringType(), True),
    StructField('force', StringType(), True),
    StructField('betInfo', StringType(), True),
    StructField('transactionTimeUtc', StringType(), True),
    StructField('referenceIdentifier', StringType(), True),
    StructField('referenceGroupIdentifier', StringType(), True),
    StructField('amount', StringType(), True),
    StructField('currencyCode', StringType(), True),
    StructField('walletType', StringType(), True),
    StructField('jackpotContribution', StringType(), True),
    StructField('bonusAmount', StringType(), True),
    StructField('oxiDirectTransaction', StringType(), True),
    StructField('balance', StringType(), True),
    StructField('gameType', StringType(), True),
])


    spark = SparkSession \
        .builder \
        .appName("churn-journey-session") \
        .master("local[3]") \
        .getOrCreate()

    riga_stream = spark \
      .readStream \
      .format("kafka") \
      .option('kafka.bootstrap.servers', 'z-2.mskwhclusternonprod.67xgww.c3.kafka.eu-west-1.amazonaws.com:2181') \
      .option("kafka.security.protocol", "SSL") \
      .option("kafka.ssl.truststore.location", "./ssl/msk/kafka.client.truststore.jks") \
      .option("kafka.ssl.keystore.location", "./ssl/msk/msk.keystore.jks") \
      .option("kafka.ssl.keystore.password", "password") \
      .option("kafka.ssl.truststore.password", "changeit") \
      .option("kafka.ssl.key.password", "password") \
      .option("startingOffsets", "latest") \
      .option('subscribe', "nonprod-riga-uk-all") \
      .load() \
      .selectExpr("cast(value as string) as message","timestamp").select(from_json(col("message"),schema).alias("json")) \
      .select("json.*")

    riga_stream\
      .writeStream\
      .format("console")\
      .outputMode("append")\
      .start() \
      .awaitTermination()