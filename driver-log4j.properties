# The driver logs will be divided into three different logs: stdout, stderr, and log4j. The stdout
# and stderr are rolled using StdoutStderrRoller. The log4j logs are again split into two: public
# and private. Stdout, stderr, and only the public log4j logs are shown to the customers.
log4j.rootCategory=INFO, publicFile

# Use the private logger method from the ConsoleLogging trait to log to the private file.
# All other logs will go to the public file.
log4j.logger.privateLog=INFO, privateFile
log4j.additivity.privateLog=false

# privateFile
log4j.appender.privateFile=com.databricks.logging.RedactionRollingFileAppender
log4j.appender.privateFile.layout=org.apache.log4j.PatternLayout
log4j.appender.privateFile.layout.ConversionPattern=%d{yy/MM/dd HH:mm:ss} %p (%t) %c: %m%n
log4j.appender.privateFile.rollingPolicy=org.apache.log4j.rolling.TimeBasedRollingPolicy
log4j.appender.privateFile.rollingPolicy.FileNamePattern=logs/%d{yyyy-MM-dd-HH}.log.gz
log4j.appender.privateFile.rollingPolicy.ActiveFileName=logs/active.log

# publicFile
log4j.appender.publicFile=com.databricks.logging.RedactionRollingFileAppender
log4j.appender.publicFile.layout=org.apache.log4j.PatternLayout
log4j.appender.publicFile.layout.ConversionPattern=%d{yy/MM/dd HH:mm:ss} %p (%t) %c: %m%n
log4j.appender.publicFile.rollingPolicy=org.apache.log4j.rolling.TimeBasedRollingPolicy
log4j.appender.publicFile.rollingPolicy.FileNamePattern=logs/log4j-%d{yyyy-MM-dd-HH}.log.gz
log4j.appender.publicFile.rollingPolicy.ActiveFileName=logs/log4j-active.log

# Increase log level of NewHadoopRDD so it doesn't print every split.
# (This is really because Parquet prints the whole schema for every part.)
log4j.logger.org.apache.spark.rdd.NewHadoopRDD=WARN

# Enable logging for Azure Data Lake (SC-14894)
log4j.logger.com.microsoft.azure.datalake.store=DEBUG
log4j.logger.com.microsoft.azure.datalake.store.HttpTransport=DEBUG
log4j.logger.com.microsoft.azure.datalake.store.HttpTransport.tokens=DEBUG
# We also add custom filter to remove excessive logging of successful http requests
log4j.appender.publicFile.filter.adl=com.databricks.logging.DatabricksLogFilter
log4j.appender.publicFile.filter.adl.LoggerName=com.microsoft.azure.datalake.store.HttpTransport
log4j.appender.publicFile.filter.adl.StringToMatch=HTTPRequest,Succeeded
log4j.appender.publicFile.filter.adl.AcceptOnMatch=false

# RecordUsage Category
log4j.logger.com.databricks.UsageLogging=INFO, usage
log4j.additivity.com.databricks.UsageLogging=false
log4j.appender.usage=org.apache.log4j.rolling.DatabricksRollingFileAppender
log4j.appender.usage.layout=org.apache.log4j.PatternLayout
log4j.appender.usage.layout.ConversionPattern=%m%n
log4j.appender.usage.rollingPolicy=org.apache.log4j.rolling.TimeBasedRollingPolicy
log4j.appender.usage.rollingPolicy.FileNamePattern=logs/%d{yyyy-MM-dd-HH}.usage.json.gz
log4j.appender.usage.rollingPolicy.ActiveFileName=logs/usage.json

# Metrics Logs
log4j.logger.com.databricks.MetricsLogging=INFO, metrics
log4j.additivity.com.databricks.MetricsLogging=false
log4j.appender.metrics=org.apache.log4j.rolling.DatabricksRollingFileAppender
log4j.appender.metrics.layout=org.apache.log4j.PatternLayout
log4j.appender.metrics.layout.ConversionPattern=%m%n
log4j.appender.metrics.rollingPolicy=org.apache.log4j.rolling.TimeBasedRollingPolicy
log4j.appender.metrics.rollingPolicy.FileNamePattern=logs/%d{yyyy-MM-dd-HH}.metrics.json.gz
log4j.appender.metrics.rollingPolicy.ActiveFileName=logs/metrics.json
log4j.appender.metrics.encoding=UTF-8

# Ignore messages below warning level from Jetty, because it's a bit verbose
#log4j.logger.org.eclipse.jetty=WARN
log4j.category.kafkashaded.org.apache.kafka.clients.consumer.internals.Fetcher=WARN
log4j.category.org.apache.spark.ContextCleaner=WARN