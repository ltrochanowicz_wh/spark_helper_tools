import psycopg2


pq_conn = psycopg2.connect(host=conf['PQ_ENDPOINT'], port=conf['PQ_PORT'], database=conf['PQ_DBNAME'],
                           user=conf['PQ_USER'], password=pq_secret)